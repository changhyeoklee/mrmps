/**
 * @file mrmps.cpp
 * @brief Example of using MrmpsIO library.
 *
 * @author Changhyeok Lee
 * @date Dec 20, 2013
 */

#include <iostream>
#include "MrmpsIO.hpp"

/// Main function
/**
 * argv[1]: path to and name of Mrmps files
 */
int main(int argc, char **argv) {
	MrmpsIO mrmps_io;
	int status;
	if (argc >= 2) {
		status = mrmps_io.readMrmps(argv[1]);
	}
	else {
		status = -1;
	}
	return status;
}