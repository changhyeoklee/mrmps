Mrmps
=====

This project is to develop the reader for MRMPS file format. 

MRMPS files describe a two-stage moment-robust linear optimization model.
It is a robust-counter of stochastic linear optimization model. The MRMPS file
formats are similar to SMPS file formats for stochastic optimization models.
It consists of the core file, time file and amibiguous file. The core file 
is a conventional MPS file for linear program, which is the determinstic counterpart.
The time file contains the time period information and the ambiguous file includes
the information about the ambiguity set: the moment conditions and the support.

The project is motivated by "smi" package in Coin-OR (https://projects.coin-or.org/Smi),
the SMPS file reader. It is inherited from the MPS file reader in CoinUtils package
in Coin-OR (https://projects.coin-or.org/CoinUtils).

This project requires the CoinUtils package. 

Cmake files are also provided.

This is a part of the research effort to develop the solver for two-stage
moment-robust linear optimization models.

Author: Changhyeok Lee (changhyeok.lee@u.northwestern.edu)