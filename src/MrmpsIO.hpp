/**
 * @file MrmpsIO.hpp
 * @brief
 *
 * @author Changhyeok Lee
 * @date Dec 20, 2013
 */

#ifndef MRMPSIO_HPP
#define MRMPSIO_HPP

// System headers
#include <math.h>
#include <vector>
#include <algorithm>

// Mrmps headers
#include "MrmpsCardReader.hpp"

class MrmpsIO: public CoinMpsIO {
	/**
	 * @class MrmpsIO
	 * @brief Interface for reading and writing Mrmps formatted files
	 */
public:
	/** @name Constructors and destructor */
	///@{

	/// Default constructor
	MrmpsIO();

	/// Destructor
	virtual ~MrmpsIO();

	///@}

	/** @name Main public member functions */
	///@{

	/// Read Mrmps files
	/**
	 * @param name name of Mrmps files
	 * @return 0 if successful. -1 if error occurs
	 */
	const int readMrmps (const char* name);

	///@}

	/** @name Accessors */
	///@{

	/// Get number of stages
	const int getNumStages () const {
		return num_stages_;
	}

	/// Get stage of each row. 0 if first-stage. 1 if second-stage
	const int* getRowStage() const {
		return &row_stage_[0];
	}

	/// Get stage of each column. 0 if first-stage. 1 if second-stage
	const int* getColStage() const {
		return &col_stage_[0];
	}

	/// Get number of rows by stage
	/**
	 * @param stage Stage. 0 if first-stage. 1 if second-stage
	 * @return Number of rows of given stage. -1 if error occurs.
	 */
	const int getNumRowsByStage (const int stage) const {
		if (stage >= 0 && stage < num_stages_)
			return num_rows_by_stage_[stage];
		else
			return -1;
	}

	/// Get row indicies of by stage
	/**
	 * @param stage Stage. 0 if first-stage. 1 if second-stage
	 * @return Pointer to the vector of row indicies of given stage. 0 if error occurs.
	 */
	const int* getRowIndiciesByStage (const int stage) const {
		if (stage >= 0 && stage < num_stages_)
			return &row_indices_by_stage_[stage][0];
		else
			return 0;
	}

	/// Get number of columns by stage
	/**
	 * @param stage Stage. 0 if first-stage. 1 if second-stage
	 * @return Number of columns of given stage. -1 if error occurs.
	 */
	const int getNumColsByStage (const int stage) const {
		if (stage >= 0 && stage < num_stages_)
			return num_cols_by_stage_[stage];
		else
			return -1;
	}

	/// Get column indicies of by stage
	/**
	 * @param stage Stage. 0 if first-stage. 1 if second-stage
	 * @return Pointer to the vector of column indicies of given stage. 0 if error occurs.
	 */
	const int* getColIndiciesByStage (const int stage) const {
		if (stage >= 0 && stage < num_stages_)
			return &col_indices_by_stage_[stage][0];
		else
			return 0;
	}

	/// Get number of uncertain coefficients
	const int getNumUcoeffs () const {
		return num_ucoeffs_;
	}

	/// Get row index associated with each uncertain coefficient
	/**
	 * -2 : if uncertain coefficient is a column lower bound \n
	 * -1 : if uncertain coefficient is a column upper bound \n
	 * 0 .. (number of rows - 1) : otherwise
	 * number of rows : if uncertain coefficient is an objective coefficient \n
	 */
	const int* getUcoeffRow () const {
		return &ucoeff_row_[0];
	}

	/// Get column index associated with each uncertain coefficient
	/**
	 * -2 : if uncertain coefficient is a row lower bound \n
	 * -1 : if uncertain coefficient is a row upper bound \n
	 * 0 .. (number of columns - 1) : otherwise
	 */
	const int* getUcoeffCol () const {
		return &ucoeff_col_[0];
	}

	/// Get number of uncertain parameters
	const int getNumUpars () const {
		return num_upars_;
	}

	/// Get number of elements in matrix of affine map from uncertain parameters to coefficients
	const int getNumUmatElements () const {
		return num_umat_elements_;
	}

	/// Get uncertain coefficiet index of each element in matrix of affine map
	const int* getUmatUcoeffIndex () const {
		return &umat_ucoeff_index_[0];
	}

	/// Get uncertain parameter index of each element in matrix of affine map
	const int* getUmatUparIndex () const {
		return &umat_upar_index_[0];
	}

	/// Get value of each element in matrix of affine map
	const double* getUmatElement () const {
		return &umat_element_[0];
	}

	/// Get nominal value of each uncertain parameter
	const double* getUparNominal () const {
		return &upar_nominal_[0];
	}

	/// Get number of moment constraints
	const int getNumMrows () const {
		return num_mrows_;
	}

	/// Get number of elements in moment constraint matrix
	const int getNumMmatElements () const {
		return num_mmat_elements_;
	}

	/// Get moment constraint index of each element in moment constraint matrix
	const int* getMmatMrowIndex () const {
		return &mmat_mrow_index_[0];
	}

	/// Get uncertain parameter index of each element in moment constraint matrix
	const int* getMmatUparIndex () const {
		return &mmat_upar_index_[0];
	}

	/// Get value of each element in moment constraint matrix
	const double* getMmatElement () const {
		return &mmat_element_[0];
	}

	/// Get lower bound of each moment constraint
	const double* getMrowLower () const {
		return &mrow_lower_[0];
	}

	/// Get upper bound of each moment constraint
	const double* getMrowUpper () const {
		return &mrow_upper_[0];
	}

	/// Get number of support constraints
	const int getNumSrows () const {
		return num_srows_;
	}

	/// Get number of elements in support constraint matrix
	const int getNumSmatElements () const {
		return num_smat_elements_;
	}

	/// Get support constraint index of each element in support constraint matrix
	const int* getSmatSrowIndex () const {
		return &smat_srow_index_[0];
	}

	/// Get uncertain parameter index of each element in support constraint matrix
	const int* getSmatUparIndex () const {
		return &smat_upar_index_[0];
	}

	/// Get value of each element in support constraint matrix
	const double* getSmatElement () const {
		return &smat_element_[0];
	}

	/// Get lower bound of each support constraint
	const double* getSrowLower () const {
		return &srow_lower_[0];
	}

	/// Get upper bound of each support constraint
	const double* getSrowUpper () const {
		return &srow_upper_[0];
	}

	/// Get lower bound of each uncertain parameter
	const double* getUparLower () const {
		return &upar_lower_[0];
	}

	/// Get upper bound of each uncertain parameter
	const double* getUparUpper () const {
		return &upar_upper_[0];
	}

	///@}

private:

	/// std::string-int map
	typedef std::map<std::string, int> NameMap;

private:
	/// Read Mrmps time file
	/**
	 * @param name  name of time file
	 * @param ext   time file extension (default: time)
	 * @return
	 */
	const int readTimeFile (const char* name, const char* ext = "time");

	/// Read Mrmps ambiguity file
	/**
	 * @param name  name of ambiguity file
	 * @param ext   ambiguity file extension (default: ambi)
	 * @return
	 */
	const int readAmbiFile (const char* name, const char* ext = "ambi");

	/** @name Disabled special member functions */
	///@{

	/// Copy constructor is disabled
	MrmpsIO (const MrmpsIO& other);

	/// Assignment operator is disabled
	MrmpsIO& operator= (const MrmpsIO& other);

	///@}

private:
	/// Mrmps card reader
	MrmpsCardReader* mrmps_card_reader_;

	/** @name Member variables related to stages */
	///@{

	/// Number of stages
	int num_stages_;

	/// Stage of each row
	std::vector<int> row_stage_;

	/// Stage of each column
	std::vector<int> col_stage_;

	/// Number of rows by stage
	std::vector<int> num_rows_by_stage_;

	/// Row indices by stage
	std::vector<std::vector<int> > row_indices_by_stage_;

	/// Number of columns by stage
	std::vector<int> num_cols_by_stage_;

	/// Column indices by stage
	std::vector<std::vector<int> > col_indices_by_stage_;

	///@}

	/** @name Member variables related to uncertain coefficients and parameters */
	///@{

	/// Number of uncertain coefficients
	int num_ucoeffs_;

	/// Name of uncertain coefficients
	NameMap ucoeff_names_;

	/// Row index associated with each uncertain coefficient
	std::vector<int> ucoeff_row_;

	/// Column index associated with each uncertain coefficient
	std::vector<int> ucoeff_col_;

	/// Number of uncertain parameters
	int num_upars_;

	/// Name of uncertain parameters
	NameMap upar_names_;

	/// Number of elements in matrix of affine map from uncertain parameters to coefficients
	int num_umat_elements_;

	/// Uncertain coefficiet index of each element in matrix of affine map
	std::vector<int> umat_ucoeff_index_;

	/// Uncertain parameter index of each element in matrix of affine map
	std::vector<int> umat_upar_index_;

	/// Value of each element in matrix of affine map
	std::vector<double> umat_element_;

	/// Nominal value of each uncertain parameter
	std::vector<double> upar_nominal_;

	///@}

	/** @name Member variables related to moment constraints */
	///@{

	/// Name of moment constraints
	NameMap mrow_names_;

	/// Number of moment constraints
	int num_mrows_;

	/// Number of elements in moment constraint matrix
	int num_mmat_elements_;

	/// Moment constraint index of each element in moment constraint matrix
	std::vector<int> mmat_mrow_index_;

	/// Uncertain parameter index of each element in moment constraint matrix
	std::vector<int> mmat_upar_index_;

	/// Value of each element in moment constraint matrix
	std::vector<double> mmat_element_;

	/// Lower bound of each moment constraint
	std::vector<double> mrow_lower_;

	/// Upper bound of each moment constraint
	std::vector<double> mrow_upper_;

	///@}

	/** @name Member variables related to support of uncertain parameters */
	///@{

	/// Name of support constraints
	NameMap srow_names_;

	/// Number of support constraints
	int num_srows_;

	/// Number of elements in support constraint matrix
	int num_smat_elements_;

	/// Support constraint index of each element in support constraint matrix
	std::vector<int> smat_srow_index_;

	/// Uncertain parameter index of each element in support constraint matrix
	std::vector<int> smat_upar_index_;

	/// Value of each element in support constraint matrix
	std::vector<double> smat_element_;

	/// Lower bound of each support constraint
	std::vector<double> srow_lower_;

	/// Upper bound of each support constraint
	std::vector<double> srow_upper_;

	/// Lower bound of each uncertain parameter
	std::vector<double> upar_lower_;

	/// Upper bound of each uncertain parameter
	std::vector<double> upar_upper_;

	///@}
};

#endif // MRMPSIO_HPP