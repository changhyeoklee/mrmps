/**
 * @file MrmpsCardReader.hpp
 * @brief
 *
 * @author Changhyeok Lee
 * @date Dec 20, 2013
 */

#ifndef MRMPSCARDREADER_HPP
#define MRMPSCARDREADER_HPP

// System headers
#include <cstdlib>

// Coin headers
#include "CoinMpsIO.hpp"
#include "CoinMessage.hpp"

/// Mrmps section types
enum MrmpsSectionType {
    MRMPS_NO_SECTION,
    MRMPS_NAME_SECTION,
    MRMPS_STAGES_SECTION,
	MRMPS_COLSTGS_SECTION,
	MRMPS_ROWSTGS_SECTION,
	MRMPS_UCOEFFS_SECTION,
    MRMPS_UPARS_SECTION,
	MRMPS_UPARMAP_SECTION,
    MRMPS_MROWS_SECTION,
    MRMPS_MCOLS_SECTION,
    MRMPS_MRHS_SECTION,
    MRMPS_MRANGES_SECTION,
    MRMPS_SROWS_SECTION,
    MRMPS_SCOLS_SECTION,
    MRMPS_SRHS_SECTION,
    MRMPS_SRANGES_SECTION,
    MRMPS_SBOUNDS_SECTION,
    MRMPS_ENDATA_SECTION,
    MRMPS_EOF_SECTION,
    MRMPS_UNKNOWN_SECTION
};

/// Mrmps card types
enum MrmpsCardType {
    MRMPS_E_ROW_CARD,
    MRMPS_L_ROW_CARD,
    MRMPS_G_ROW_CARD,
    MRMPS_LO_BOUND_CARD,
    MRMPS_UP_BOUND_CARD,
    MRMPS_FX_BOUND_CARD,
    MRMPS_FR_BOUND_CARD,
    MRMPS_MI_BOUND_CARD,
    MRMPS_PL_BOUND_CARD,
    MRMPS_BLANK_COLUMN_CARD,
    MRMPS_UNKNOWN_CARD
};

class MrmpsCardReader : public CoinMpsCardReader {
	/**
	 * @class MrmpsCardReader
	 * @brief Mrmps card reader
	 */
public:
	/** @name Constructors and destructor */
	///@{

	/// Constructor with file to open
	/**
	 * @param input File to open
	 * @param reader Pointer to CoinMpsIO object used to intialize base class
	 */
	MrmpsCardReader (CoinFileInput* input, CoinMpsIO* reader);

	/// Destructor
	virtual ~MrmpsCardReader();

	///@}

	/** @name Main public member functions */
	///@{

	/// Read next field and return section type
	const MrmpsSectionType nextMrmpsField();

	///@}

	/** @name Accessors */
	///@{

	/// Return current section type
	const MrmpsSectionType whichMrmpsSection() const {
		return mrmps_section_type_;
	}

	/// Return current card type
	const MrmpsCardType whichMrmpsCard() const {
		return mrmps_card_type_;
	}

	/// Return current card name
	const char* cardName () const {
		return card_name_;
	}

	///@}

private:
	/** @name Disabled special member functions */
	///@{

	/// Copy constructor is disabled
	MrmpsCardReader (const MrmpsCardReader& other);

	/// Assignment operator is disabled
	MrmpsCardReader& operator= (const MrmpsCardReader& other);

	///@}

private:
	/// Current section type
	MrmpsSectionType mrmps_section_type_;

	/// Current card type
	MrmpsCardType mrmps_card_type_;

	/// Current card name
	char card_name_[COIN_MAX_FIELD_LENGTH];
};

#endif // MRMPSCARDREADER_HPP