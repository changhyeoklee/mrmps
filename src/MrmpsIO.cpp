/**
 * @file MrmpsIO.cpp
 * @brief
 *
 * @author Changhyeok Lee
 * @date Dec 23, 2013
 */

#include "MrmpsIO.hpp"

MrmpsIO::MrmpsIO() :
	mrmps_card_reader_ (0),
	num_stages_ (0),
	row_stage_(),
	col_stage_(),
	num_rows_by_stage_(),
	row_indices_by_stage_(),
	num_cols_by_stage_(),
	col_indices_by_stage_(),
	num_ucoeffs_(0),
	ucoeff_names_(),
	ucoeff_row_(),
	ucoeff_col_(),
	num_upars_(0),
	upar_names_(),
	num_umat_elements_ (0),
	umat_ucoeff_index_(),
	umat_upar_index_(),
	umat_element_(),
	upar_nominal_(),
	mrow_names_(),
	num_mrows_ (0),
	num_mmat_elements_ (0),
	mmat_mrow_index_(),
	mmat_upar_index_(),
	mmat_element_(),
	mrow_lower_(),
	mrow_upper_(),
	srow_names_(),
	num_srows_ (0),
	num_smat_elements_ (0),
	smat_srow_index_(),
	smat_upar_index_(),
	smat_element_(),
	srow_lower_(),
	srow_upper_(),
	upar_lower_(),
	upar_upper_() {
}

MrmpsIO::~MrmpsIO() {
	if (mrmps_card_reader_) {
		delete mrmps_card_reader_;
		mrmps_card_reader_ = 0;
	}
}

const int MrmpsIO::readMrmps (const char* name) {
	const char* core_ext = "core";
	const char* time_ext = "time";
	const char* ambi_ext = "ambi";

	std::string file_name (name);
	std::string full_name;

	// Check if core file is readable.
	full_name = file_name + "." + core_ext;
	if (!fileCoinReadable (full_name)) {
		std::cerr << "MrmpsIO::readMrmps(): " << full_name
		          << " cannot be found." << std::endl;
		return -1;
	}

	// Read core file.
	std::cout << ">> Start reading core file." << std::endl;
	if (readMps (name, core_ext) != 0) {
		return -1;
	}

	// Check if time file is readable.
	full_name = file_name + "." + time_ext;
	if (!fileCoinReadable (full_name)) {
		std::cerr << "MrmpsIO::readMrmps(): " << full_name
		          << " cannot be found." << std::endl;
		return -1;
	}

	// Read time file.
	std::cout << ">> Start reading time file." << std::endl;
	if (readTimeFile (name, time_ext) != 0) {
		return -1;
	}

	// Check if ambi file is readable.
	full_name = file_name + "." + ambi_ext;
	if (!fileCoinReadable (full_name)) {
		std::cerr << "MrmpsIO::readMrmps(): " << full_name
		          << " cannot be found." << std::endl;
		return -1;
	}

	// Read ambi file.
	std::cout << ">> Start reading ambi file." << std::endl;
	if (readAmbiFile (name, ambi_ext) != 0) {
		return -1;
	}

	std::cout << ">> Finish reading Mrmps files." << std::endl;

	// If one reaches here, then reading Mrmps files was successful.
	return 0;
}

const int MrmpsIO::readTimeFile (const char* name, const char* ext) {
	// Instantiate Mrmps card reader if necessary
	CoinFileInput* input = 0;
	int returnCode = dealWithFileName (name, ext, input);
	if (returnCode < 0) {
		return -1;
	}
	else if (returnCode > 0) {
		delete mrmps_card_reader_;
		mrmps_card_reader_ = new MrmpsCardReader (input, this);
	}

	// Try reading name section
	mrmps_card_reader_->readToNextSection();

	if (mrmps_card_reader_->whichSection() == COIN_NAME_SECTION) {
		// Read problem name and issue warning if needed
		if (strcmp (problemName_, mrmps_card_reader_->columnName())) {
			std::cout << "Warning: time file problem name "
			          << mrmps_card_reader_->columnName()
			          << " does not match core file problem name "
			          << problemName_ << std::endl;
		}
	}
	else if (mrmps_card_reader_->whichSection() == COIN_UNKNOWN_SECTION) {
		handler_->message (COIN_MPS_BADFILE1, messages_)
		        << mrmps_card_reader_->card() << 1 << fileName_
		        << CoinMessageEol;
		if (mrmps_card_reader_->fileInput()->getReadType() != "plain") {
			handler_->message (COIN_MPS_BADFILE2, messages_)
			        << mrmps_card_reader_->fileInput()->getReadType()
			        << CoinMessageEol;
		}
		return -2;
	}
	else if (mrmps_card_reader_->whichSection() != COIN_EOF_SECTION) {
		handler_->message (COIN_MPS_BADFILE1, messages_)
		        << mrmps_card_reader_->card() << 1 << fileName_
		        << CoinMessageEol;
		return -3;
	}
	else {
		handler_->message (COIN_MPS_EOF, messages_) << fileName_
		        << CoinMessageEol;
		return -4;
	}

	int num_errors = 0;

	// Try reading STAGES or COLSTGS section
	mrmps_card_reader_->nextMrmpsField();

	// If next card is STAGES section
	if (mrmps_card_reader_->whichMrmpsSection() == MRMPS_STAGES_SECTION) {
		int last_col_index = 0;
		int last_row_index = 0;
		num_stages_ = -1;

		col_stage_.resize(getNumCols(), -1);
		row_stage_.resize(getNumRows(), -1);

		while (mrmps_card_reader_->nextMrmpsField() == MRMPS_STAGES_SECTION) {
			switch (mrmps_card_reader_->whichMrmpsCard()) {
				case MRMPS_BLANK_COLUMN_CARD: {
					int col_index = columnIndex (mrmps_card_reader_->columnName());
					int row_index = rowIndex (mrmps_card_reader_->rowName());

					if (col_index >= 0) {
						if (row_index >= 0) {
							for (int i = last_col_index; i < col_index; i++) {
								col_stage_[i] = num_stages_;
							}
							for (int i = last_row_index; i < row_index; i++) {
								row_stage_[i] = num_stages_;
							}
							num_stages_++;
							last_col_index = col_index;
							last_row_index = row_index;
						}
						// Error: no matching row for stage
						else {
							num_errors++;
							if (num_errors < 100) {
								handler_->message (COIN_MPS_NOMATCHROW, messages_)
								<< mrmps_card_reader_->rowName()
								<< mrmps_card_reader_->cardNumber()
								<< mrmps_card_reader_->card()
								<< CoinMessageEol;
							}
							else if (num_errors > 100000) {
								handler_->message (COIN_MPS_RETURNING, messages_)
								<< CoinMessageEol;
								return num_errors;
							}
						}
					}
					// Error: no matching column for stage
					else {
						num_errors++;
						if (num_errors < 100) {
							handler_->message (COIN_MPS_NOMATCHCOL, messages_)
							<< mrmps_card_reader_->columnName()
							<< mrmps_card_reader_->cardNumber()
							<< mrmps_card_reader_->card()
							<< CoinMessageEol;
						}
						else if (num_errors > 100000) {
							handler_->message (COIN_MPS_RETURNING, messages_)
							<< CoinMessageEol;
							return num_errors;
						}
					}

					break;
				}
				// Error: bad image
				default: {
					num_errors++;
					if (num_errors < 100) {
						handler_->message (COIN_MPS_BADIMAGE, messages_)
						<< mrmps_card_reader_->cardNumber()
						<< mrmps_card_reader_->card() << CoinMessageEol;
					}
					else if (num_errors > 100000) {
						handler_->message (COIN_MPS_RETURNING, messages_)
						<< CoinMessageEol;
						return num_errors;
					}
				}
			}
		}
		for (int i = last_col_index; i < numberColumns_; i++) {
			col_stage_[i] = num_stages_;
		}
		for (int i = last_row_index; i < numberRows_; i++) {
			row_stage_[i] = num_stages_;
		}
		num_stages_++;
	}
	// If next card is COLSTGS section
	else if (mrmps_card_reader_->whichMrmpsSection() == MRMPS_COLSTGS_SECTION) {
		col_stage_.resize(getNumCols(), -1);
		num_stages_ = 0;

		while (mrmps_card_reader_->nextMrmpsField() == MRMPS_COLSTGS_SECTION) {
			switch (mrmps_card_reader_->whichMrmpsCard()) {
				case MRMPS_BLANK_COLUMN_CARD: {
					int col_index = columnIndex (mrmps_card_reader_->columnName());

					if (col_index >= 0) {
						col_stage_[col_index] = mrmps_card_reader_->value()-1;
						if (mrmps_card_reader_->value() > num_stages_) {
							num_stages_ = mrmps_card_reader_->value();
						}
					}
					// Error: no matching column for stage
					else {
						num_errors++;
						if (num_errors < 100) {
							handler_->message (COIN_MPS_NOMATCHCOL, messages_)
							<< mrmps_card_reader_->columnName()
							<< mrmps_card_reader_->cardNumber()
							<< mrmps_card_reader_->card()
							<< CoinMessageEol;
						}
						else if (num_errors > 100000) {
							handler_->message (COIN_MPS_RETURNING, messages_)
							<< CoinMessageEol;
							return num_errors;
						}
					}

					break;
				}
				// Error: bad image
				default: {
					num_errors++;
					if (num_errors < 100) {
						handler_->message (COIN_MPS_BADIMAGE, messages_)
						<< mrmps_card_reader_->cardNumber()
						<< mrmps_card_reader_->card() << CoinMessageEol;
					}
					else if (num_errors > 100000) {
						handler_->message (COIN_MPS_RETURNING, messages_)
						<< CoinMessageEol;
						return num_errors;
					}
				}
			}
		}

		// Try reading ROWSTGS section
		if (mrmps_card_reader_->whichMrmpsSection() != MRMPS_ROWSTGS_SECTION) {
			handler_->message (COIN_MPS_BADIMAGE, messages_)
			<< mrmps_card_reader_->cardNumber()
			<< mrmps_card_reader_->card() << CoinMessageEol;
			handler_->message (COIN_MPS_RETURNING, messages_) << CoinMessageEol;
			return num_errors + 100000;
		}

		row_stage_.resize(getNumRows(), -1);

		while (mrmps_card_reader_->nextMrmpsField() == MRMPS_ROWSTGS_SECTION) {
			switch (mrmps_card_reader_->whichMrmpsCard()) {
				case MRMPS_BLANK_COLUMN_CARD: {
					int row_index = rowIndex (mrmps_card_reader_->rowName());

					if (row_index >= 0) {
						row_stage_[row_index] = mrmps_card_reader_->value()-1;
						if (mrmps_card_reader_->value() > num_stages_) {
							num_stages_ = mrmps_card_reader_->value();
						}
					}
					// Error: no matching row for stage
					else {
						num_errors++;
						if (num_errors < 100) {
							handler_->message (COIN_MPS_NOMATCHROW, messages_)
							<< mrmps_card_reader_->rowName()
							<< mrmps_card_reader_->cardNumber()
							<< mrmps_card_reader_->card()
							<< CoinMessageEol;
						}
						else if (num_errors > 100000) {
							handler_->message (COIN_MPS_RETURNING, messages_)
							<< CoinMessageEol;
							return num_errors;
						}
					}

					break;
				}
				// Error: bad image
				default: {
					num_errors++;
					if (num_errors < 100) {
						handler_->message (COIN_MPS_BADIMAGE, messages_)
						<< mrmps_card_reader_->cardNumber()
						<< mrmps_card_reader_->card() << CoinMessageEol;
					}
					else if (num_errors > 100000) {
						handler_->message (COIN_MPS_RETURNING, messages_)
						<< CoinMessageEol;
						return num_errors;
					}
				}
			}
		}
	}
	// Error: bad image
	else {
		handler_->message (COIN_MPS_BADIMAGE, messages_)
		<< mrmps_card_reader_->cardNumber()
		<< mrmps_card_reader_->card() << CoinMessageEol;
		handler_->message (COIN_MPS_RETURNING, messages_) << CoinMessageEol;
		return num_errors + 100000;
	}

	// Try reading ENDATA section
	if (mrmps_card_reader_->whichMrmpsSection() != MRMPS_ENDATA_SECTION) {
		handler_->message (COIN_MPS_BADIMAGE, messages_)
		<< mrmps_card_reader_->cardNumber()
		<< mrmps_card_reader_->card() << CoinMessageEol;
		handler_->message (COIN_MPS_RETURNING, messages_) << CoinMessageEol;
		return num_errors + 100000;
	}

	if (num_stages_ >= 1) {
		num_rows_by_stage_.resize(num_stages_,0);
		row_indices_by_stage_.resize(num_stages_);
		num_cols_by_stage_.resize(num_stages_,0);
		col_indices_by_stage_.resize(num_stages_);
		for (int i = 0; i < num_stages_; i++) {
			row_indices_by_stage_[i].reserve(getNumRows());
			col_indices_by_stage_[i].reserve(getNumCols());
		}

		for (int i = 0; i < row_stage_.size(); i++) {
			int stage = row_stage_[i];
			if (stage >= 0 && stage < num_stages_) {
				row_indices_by_stage_[stage].push_back(i);
				num_rows_by_stage_[stage]++;
			}
			// Error: no row stage
			else {
				std::cerr << "Stage of " << rowName(i) << " is not specified." << std::endl;
				handler_->message (COIN_MPS_RETURNING, messages_) << CoinMessageEol;
				return num_errors + 100000;
			}
		}

		for (int i = 0; i < col_stage_.size(); i++) {
			int stage = col_stage_[i];
			if (stage >= 0 && stage < num_stages_) {
				col_indices_by_stage_[stage].push_back(i);
				num_cols_by_stage_[stage]++;
			}
			// Error: no column stage
			else {
				std::cerr << "Stage of " << columnName(i) << " is not specified." << std::endl;
				handler_->message (COIN_MPS_RETURNING, messages_) << CoinMessageEol;
				return num_errors + 100000;
			}
		}

		// Sort
		for (int i = 0; i < num_stages_; i++) {
			std::sort (row_indices_by_stage_[i].begin(), row_indices_by_stage_[i].end());
			std::sort (col_indices_by_stage_[i].begin(), col_indices_by_stage_[i].end());
		}
	}
	// Error: no stage
	else {
		std::cerr << "No stage is specified." << std::endl;
		handler_->message (COIN_MPS_RETURNING, messages_) << CoinMessageEol;
		return num_errors + 100000;
	}

	return num_errors;
}

const int MrmpsIO::readAmbiFile (const char* name, const char* ext) {
	// Instantiate Mrmps card reader if necessary
	CoinFileInput* input = 0;
	int returnCode = dealWithFileName (name, ext, input);
	if (returnCode < 0) {
		return -1;
	}
	else if (returnCode > 0) {
		delete mrmps_card_reader_;
		mrmps_card_reader_ = new MrmpsCardReader (input, this);
	}

	// Try reading name section
	mrmps_card_reader_->readToNextSection();

	if (mrmps_card_reader_->whichSection() == COIN_NAME_SECTION) {
		// Check problem name and issue warning if needed
		if (strcmp (problemName_, mrmps_card_reader_->columnName())) {
			std::cout << "Warning: ambi file problem name "
			          << mrmps_card_reader_->columnName()
			          << " does not match core file problem name "
			          << problemName_ << std::endl;
		}
	}
	else if (mrmps_card_reader_->whichSection() == COIN_UNKNOWN_SECTION) {
		handler_->message (COIN_MPS_BADFILE1, messages_)
		        << mrmps_card_reader_->card() << 1 << fileName_
		        << CoinMessageEol;
		if (mrmps_card_reader_->fileInput()->getReadType() != "plain") {
			handler_->message (COIN_MPS_BADFILE2, messages_)
			        << mrmps_card_reader_->fileInput()->getReadType()
			        << CoinMessageEol;
		}
		return -2;
	}
	else if (mrmps_card_reader_->whichSection() != COIN_EOF_SECTION) {
		handler_->message (COIN_MPS_BADFILE1, messages_)
		        << mrmps_card_reader_->card() << 1 << fileName_
		        << CoinMessageEol;
		return -3;
	}
	else {
		handler_->message (COIN_MPS_EOF, messages_) << fileName_
		        << CoinMessageEol;
		return -4;
	}

	int num_errors = 0;

	// Try reading UCOEFFS section
	mrmps_card_reader_->nextMrmpsField();

	if (mrmps_card_reader_->whichMrmpsSection() != MRMPS_UCOEFFS_SECTION) {
		handler_->message (COIN_MPS_BADIMAGE, messages_)
		        << mrmps_card_reader_->cardNumber()
		        << mrmps_card_reader_->card() << CoinMessageEol;
		handler_->message (COIN_MPS_RETURNING, messages_) << CoinMessageEol;
		return num_errors + 100000;
	}

	int max_ucoeffs = 1000;
	ucoeff_col_.reserve (max_ucoeffs);
	ucoeff_row_.reserve (max_ucoeffs);

	const char* ub = "UB";
	const char* lb = "LB";

	while (mrmps_card_reader_->nextMrmpsField() == MRMPS_UCOEFFS_SECTION) {
		switch (mrmps_card_reader_->whichMrmpsCard()) {
			case MRMPS_BLANK_COLUMN_CARD: {
				char col_name[COIN_MAX_FIELD_LENGTH];
				char row_name[COIN_MAX_FIELD_LENGTH];
				strcpy (col_name, mrmps_card_reader_->columnName());
				strcpy (row_name, mrmps_card_reader_->rowName());

				int col_index = columnIndex (col_name);
				int row_index = rowIndex (row_name);

				if (col_index >= 0 || (strcmp (col_name, ub) == 0)
				        || (strcmp (col_name, lb) == 0)) {
					if (row_index >= 0 ||
					        ( ( (strcmp (row_name, ub) == 0)
					            || (strcmp (row_name, lb) == 0))
					          && (col_index >= 0))) {
						// If capacity is reached, reserve memory
						if (num_ucoeffs_ >= max_ucoeffs) {
							max_ucoeffs = (3 * max_ucoeffs) / 2 + 1000;
							ucoeff_col_.reserve (max_ucoeffs);
							ucoeff_row_.reserve (max_ucoeffs);
						}

						if (strcmp (col_name, ub) == 0) {
							ucoeff_col_.push_back (-1);
							ucoeff_row_.push_back (row_index);

						}
						else if (strcmp (col_name, lb) == 0) {
							ucoeff_col_.push_back (-2);
							ucoeff_row_.push_back (row_index);

						}
						else if (strcmp (row_name, ub) == 0) {
							ucoeff_col_.push_back (col_index);
							ucoeff_row_.push_back (-1);

						}
						else if (strcmp (row_name, lb) == 0) {
							ucoeff_col_.push_back (col_index);
							ucoeff_row_.push_back (-2);

						}
						else {
							ucoeff_col_.push_back (col_index);
							ucoeff_row_.push_back (row_index);
						}

						ucoeff_names_.insert (std::pair<std::string, int>
						                    (mrmps_card_reader_->cardName(), num_ucoeffs_));
						num_ucoeffs_++;
					}
					// Error: no matching row for ucoeff
					else {
						num_errors++;
						if (num_errors < 100) {
							handler_->message (COIN_MPS_NOMATCHROW, messages_)
							        << mrmps_card_reader_->rowName()
							        << mrmps_card_reader_->cardNumber()
							        << mrmps_card_reader_->card()
							        << CoinMessageEol;
						}
						else if (num_errors > 100000) {
							handler_->message (COIN_MPS_RETURNING, messages_)
							        << CoinMessageEol;
							return num_errors;
						}
					}
				}
				// Error: no matching column for ucoeff
				else {
					num_errors++;
					if (num_errors < 100) {
						handler_->message (COIN_MPS_NOMATCHCOL, messages_)
						        << mrmps_card_reader_->columnName()
						        << mrmps_card_reader_->cardNumber()
						        << mrmps_card_reader_->card()
						        << CoinMessageEol;
					}
					else if (num_errors > 100000) {
						handler_->message (COIN_MPS_RETURNING, messages_)
						        << CoinMessageEol;
						return num_errors;
					}
				}

				break;
			}
			// Error: bad image
			default: {
				num_errors++;
				if (num_errors < 100) {
					handler_->message (COIN_MPS_BADIMAGE, messages_)
					        << mrmps_card_reader_->cardNumber()
					        << mrmps_card_reader_->card() << CoinMessageEol;
				}
				else if (num_errors > 100000) {
					handler_->message (COIN_MPS_RETURNING, messages_)
					        << CoinMessageEol;
					return num_errors;
				}
			}
		}
	}

	// Try reading UPARS section
	if (mrmps_card_reader_->whichMrmpsSection() != MRMPS_UPARS_SECTION) {
		handler_->message (COIN_MPS_BADIMAGE, messages_)
		<< mrmps_card_reader_->cardNumber()
		<< mrmps_card_reader_->card() << CoinMessageEol;
		handler_->message (COIN_MPS_RETURNING, messages_) << CoinMessageEol;
		return num_errors + 100000;
	}

	int max_upars = 1000;
	upar_nominal_.reserve(max_upars);

	while (mrmps_card_reader_->nextMrmpsField() == MRMPS_UPARS_SECTION) {
		switch (mrmps_card_reader_->whichMrmpsCard()) {
			case MRMPS_BLANK_COLUMN_CARD: {
				// If capacity is reached, reserve memory
				if (num_upars_ >= max_upars) {
					max_upars = (3 * max_upars) / 2 + 1000;
					upar_nominal_.reserve (max_upars);
				}

				upar_nominal_.push_back (mrmps_card_reader_->value());
				upar_names_.insert (std::pair<std::string, int>
				                    (mrmps_card_reader_->cardName(), num_upars_));
				num_upars_++;

				break;
			}
			// Error: bad image
			default: {
				num_errors++;
				if (num_errors < 100) {
					handler_->message (COIN_MPS_BADIMAGE, messages_)
					<< mrmps_card_reader_->cardNumber()
					<< mrmps_card_reader_->card() << CoinMessageEol;
				}
				else if (num_errors > 100000) {
					handler_->message (COIN_MPS_RETURNING, messages_)
					<< CoinMessageEol;
					return num_errors;
				}
			}
		}
	}

	// Try reading UPARMAP section
	if (mrmps_card_reader_->whichMrmpsSection() != MRMPS_UPARMAP_SECTION) {
		handler_->message (COIN_MPS_BADIMAGE, messages_)
		<< mrmps_card_reader_->cardNumber()
		<< mrmps_card_reader_->card() << CoinMessageEol;
		handler_->message (COIN_MPS_RETURNING, messages_) << CoinMessageEol;
		return num_errors + 100000;
	}

	const char* nominal = "NOMINAL";

	num_umat_elements_ = 0;
	int max_umat_elements = num_ucoeffs_ * 2;

	umat_ucoeff_index_.reserve (max_umat_elements);
	umat_upar_index_.reserve (max_umat_elements);
	umat_element_.reserve (max_umat_elements);

	while (mrmps_card_reader_->nextMrmpsField() == MRMPS_UPARMAP_SECTION) {
		switch (mrmps_card_reader_->whichMrmpsCard()) {
			case MRMPS_BLANK_COLUMN_CARD: {
				// Try getting upar index
				NameMap::iterator upar;
				upar = upar_names_.find (mrmps_card_reader_->columnName());

				if (upar != upar_names_.end()) {
					// Try getting ucoeff index
					NameMap::iterator ucoeff;
					ucoeff = ucoeff_names_.find (mrmps_card_reader_->rowName());

					if (ucoeff != ucoeff_names_.end()) {
						// If value is meaningful
						if (fabs (mrmps_card_reader_->value()) > smallElement_) {
							// If capacity is reached, reserve memory
							if (num_umat_elements_ >= max_umat_elements) {
								max_umat_elements = (3 * max_umat_elements) / 2 + 1000;
								umat_ucoeff_index_.reserve (max_umat_elements);
								umat_upar_index_.reserve (max_umat_elements);
								umat_element_.reserve (max_umat_elements);
							}

							umat_ucoeff_index_.push_back (ucoeff->second);
							umat_upar_index_.push_back (upar->second);
							umat_element_.push_back (mrmps_card_reader_->value());
							num_umat_elements_++;
						}
					}
					// Error: no matching ucoeff
					else {
						num_errors++;
						if (num_errors < 100) {
							handler_->message (COIN_MPS_NOMATCHROW, messages_)
							<< mrmps_card_reader_->rowName()
							<< mrmps_card_reader_->cardNumber()
							<< mrmps_card_reader_->card()
							<< CoinMessageEol;
						}
						else if (num_errors > 100000) {
							handler_->message (COIN_MPS_RETURNING, messages_)
							<< CoinMessageEol;
							return num_errors;
						}
					}
				}
				// Error: no matching upar
				else {
					num_errors++;
					if (num_errors < 100) {
						handler_->message (COIN_MPS_NOMATCHCOL, messages_)
						<< mrmps_card_reader_->columnName()
						<< mrmps_card_reader_->cardNumber()
						<< mrmps_card_reader_->card()
						<< CoinMessageEol;
					}
					else if (num_errors > 100000) {
						handler_->message (COIN_MPS_RETURNING, messages_)
						<< CoinMessageEol;
						return num_errors;
					}
				}

				break;
			}
			// Error: bad image
			default: {
				num_errors++;
				if (num_errors < 100) {
					handler_->message (COIN_MPS_BADIMAGE, messages_)
					<< mrmps_card_reader_->cardNumber()
					<< mrmps_card_reader_->card() << CoinMessageEol;
				}
				else if (num_errors > 100000) {
					handler_->message (COIN_MPS_RETURNING, messages_)
					<< CoinMessageEol;
					return num_errors;
				}
			}
		}
	}

	// Try reading MROWS section
	if (mrmps_card_reader_->whichMrmpsSection() != MRMPS_MROWS_SECTION) {
		handler_->message (COIN_MPS_BADIMAGE, messages_)
		        << mrmps_card_reader_->cardNumber()
		        << mrmps_card_reader_->card() << CoinMessageEol;
		handler_->message (COIN_MPS_RETURNING, messages_) << CoinMessageEol;
		return num_errors + 100000;
	}

	int max_mrows = 1000;
	std::vector<MrmpsCardType> mrow_types;
	mrow_types.reserve (max_mrows);

	while (mrmps_card_reader_->nextMrmpsField() == MRMPS_MROWS_SECTION) {
		switch (mrmps_card_reader_->whichMrmpsCard()) {
			case MRMPS_E_ROW_CARD:
			case MRMPS_L_ROW_CARD:
			case MRMPS_G_ROW_CARD: {
				// If capacity is reached, reserve memory
				if (num_mrows_ >= max_mrows) {
					max_mrows = (3 * max_mrows) / 2 + 1000;
					mrow_types.reserve (max_mrows);
				}

				mrow_types.push_back (mrmps_card_reader_->whichMrmpsCard());
				mrow_names_.insert (std::pair<std::string, int>
				                    (mrmps_card_reader_->cardName(), num_mrows_));
				num_mrows_++;

				break;
			}
			// Error: bad image
			default: {
				num_errors++;
				if (num_errors < 100) {
					handler_->message (COIN_MPS_BADIMAGE, messages_)
					        << mrmps_card_reader_->cardNumber()
					        << mrmps_card_reader_->card() << CoinMessageEol;
				}
				else if (num_errors > 100000) {
					handler_->message (COIN_MPS_RETURNING, messages_)
					        << CoinMessageEol;
					return num_errors;
				}
			}
		}
	}

	// Try reading MCOLS section
	if (mrmps_card_reader_->whichMrmpsSection() != MRMPS_MCOLS_SECTION) {
		handler_->message (COIN_MPS_BADIMAGE, messages_)
		        << mrmps_card_reader_->cardNumber()
		        << mrmps_card_reader_->card() << CoinMessageEol;
		handler_->message (COIN_MPS_RETURNING, messages_) << CoinMessageEol;
		return num_errors + 100000;
	}

	num_mmat_elements_ = 0;
	int max_mmat_elements = num_mrows_ * 2;

	mmat_mrow_index_.reserve (max_mmat_elements);
	mmat_upar_index_.reserve (max_mmat_elements);
	mmat_element_.reserve (max_mmat_elements);

	while (mrmps_card_reader_->nextMrmpsField() == MRMPS_MCOLS_SECTION) {
		switch (mrmps_card_reader_->whichMrmpsCard()) {
			case MRMPS_BLANK_COLUMN_CARD: {
				// Try getting upar index
				NameMap::iterator upar;
				upar = upar_names_.find (mrmps_card_reader_->columnName());

				if (upar != upar_names_.end()) {
					// Try getting mrow index
					NameMap::iterator mrow;
					mrow = mrow_names_.find (mrmps_card_reader_->rowName());

					if (mrow != mrow_names_.end()) {
						// If value is meaningful
						if (fabs (mrmps_card_reader_->value()) > smallElement_) {
							// If capacity is reached, reserve memory
							if (num_mmat_elements_ >= max_mmat_elements) {
								max_mmat_elements = (3 * max_mmat_elements) / 2 + 1000;
								mmat_mrow_index_.reserve (max_mmat_elements);
								mmat_upar_index_.reserve (max_mmat_elements);
								mmat_element_.reserve (max_mmat_elements);
							}

							mmat_mrow_index_.push_back (mrow->second);
							mmat_upar_index_.push_back (upar->second);
							mmat_element_.push_back (mrmps_card_reader_->value());
							num_mmat_elements_++;
						}
					}
					// Error: no matching mrow
					else {
						num_errors++;
						if (num_errors < 100) {
							handler_->message (COIN_MPS_NOMATCHROW, messages_)
							        << mrmps_card_reader_->rowName()
							        << mrmps_card_reader_->cardNumber()
							        << mrmps_card_reader_->card()
							        << CoinMessageEol;
						}
						else if (num_errors > 100000) {
							handler_->message (COIN_MPS_RETURNING, messages_)
							        << CoinMessageEol;
							return num_errors;
						}
					}
				}
				// Error: no matching upar
				else {
					num_errors++;
					if (num_errors < 100) {
						handler_->message (COIN_MPS_NOMATCHCOL, messages_)
						        << mrmps_card_reader_->columnName()
						        << mrmps_card_reader_->cardNumber()
						        << mrmps_card_reader_->card()
						        << CoinMessageEol;
					}
					else if (num_errors > 100000) {
						handler_->message (COIN_MPS_RETURNING, messages_)
						        << CoinMessageEol;
						return num_errors;
					}
				}

				break;
			}
			// Error: bad image
			default: {
				num_errors++;
				if (num_errors < 100) {
					handler_->message (COIN_MPS_BADIMAGE, messages_)
					        << mrmps_card_reader_->cardNumber()
					        << mrmps_card_reader_->card() << CoinMessageEol;
				}
				else if (num_errors > 100000) {
					handler_->message (COIN_MPS_RETURNING, messages_)
					        << CoinMessageEol;
					return num_errors;
				}
			}
		}
	}

	// Initialize mrow bounds
	mrow_lower_ = std::vector<double> (num_mrows_, -infinity_);
	mrow_upper_ = std::vector<double> (num_mrows_, infinity_);

	// If there is MRHS section
	if (mrmps_card_reader_->whichMrmpsSection() == MRMPS_MRHS_SECTION) {
		while (mrmps_card_reader_->nextMrmpsField() == MRMPS_MRHS_SECTION) {
			switch (mrmps_card_reader_->whichMrmpsCard()) {
				case MRMPS_BLANK_COLUMN_CARD: {
					// Try getting mrow index
					NameMap::iterator mrow;
					mrow = mrow_names_.find (mrmps_card_reader_->rowName());

					if (mrow != mrow_names_.end()) {
						int mrow_index = mrow->second;
						int value = mrmps_card_reader_->value();

						// Check for duplicates
						if ( (mrow_lower_[mrow_index] == -infinity_)
						        && (mrow_upper_[mrow_index] == infinity_)) {
							// If value is meaningful
							if (fabs (mrmps_card_reader_->value()) > smallElement_) {
								switch (mrow_types[mrow_index]) {
									case MRMPS_E_ROW_CARD: {
										mrow_lower_[mrow_index] = value;
										mrow_upper_[mrow_index] = value;
										break;
									}
									case MRMPS_L_ROW_CARD: {
										mrow_upper_[mrow_index] = value;
										break;
									}
									case MRMPS_G_ROW_CARD: {
										mrow_lower_[mrow_index] = value;
									}
								}
							}
						}
						// Error: duplicated mrhs
						else {
							if (num_errors < 100) {
								handler_->message (COIN_MPS_DUPROW, messages_)
								        << mrmps_card_reader_->rowName()
								        << mrmps_card_reader_->cardNumber()
								        << mrmps_card_reader_->card()
								        << CoinMessageEol;
							}
							else if (num_errors > 100000) {
								handler_->message (COIN_MPS_RETURNING, messages_)
								        << CoinMessageEol;
								return num_errors;
							}
						}
					}
					// Error: no matching mrow
					else {
						num_errors++;
						if (num_errors < 100) {
							handler_->message (COIN_MPS_NOMATCHROW, messages_)
							        << mrmps_card_reader_->rowName()
							        << mrmps_card_reader_->cardNumber()
							        << mrmps_card_reader_->card()
							        << CoinMessageEol;
						}
						else if (num_errors > 100000) {
							handler_->message (COIN_MPS_RETURNING, messages_)
							        << CoinMessageEol;
							return num_errors;
						}
					}

					break;
				}
				// Error: bad image
				default: {
					num_errors++;
					if (num_errors < 100) {
						handler_->message (COIN_MPS_BADIMAGE, messages_)
						        << mrmps_card_reader_->cardNumber()
						        << mrmps_card_reader_->card() << CoinMessageEol;
					}
					else if (num_errors > 100000) {
						handler_->message (COIN_MPS_RETURNING, messages_)
						        << CoinMessageEol;
						return num_errors;
					}
				}
			}
		}
	}

	// Fill zero for blank mrhs's
	for (int i = 0; i < num_mrows_; i++) {
		switch (mrow_types[i]) {
			case COIN_E_ROW: {
				if (mrow_upper_[i] == infinity_ && mrow_lower_[i] == -infinity_) {
					mrow_upper_[i] = 0.0;
					mrow_lower_[i] = 0.0;
				}
				break;
			}
			case COIN_L_ROW: {
				if (mrow_upper_[i] == infinity_) {
					mrow_upper_[i] = 0.0;
				}
				break;
			}
			case COIN_G_ROW: {
				if (mrow_lower_[i] == -infinity_) {
					mrow_lower_[i] = 0.0;
				}
				break;
			}
		}
	}

	// If there is MRANGES section
	if (mrmps_card_reader_->whichMrmpsSection() == MRMPS_MRANGES_SECTION) {
		while (mrmps_card_reader_->nextMrmpsField() == MRMPS_MRANGES_SECTION) {
			switch (mrmps_card_reader_->whichMrmpsCard()) {
				case MRMPS_BLANK_COLUMN_CARD: {
					// Try getting mrow index
					NameMap::iterator mrow;
					mrow = mrow_names_.find (mrmps_card_reader_->rowName());

					if (mrow != mrow_names_.end()) {
						int mrow_index = mrow->second;
						int value = mrmps_card_reader_->value();

						// Check for duplicates
						bool duplicate = false;
						switch (mrow_types[mrow_index]) {
							case MRMPS_L_ROW_CARD: {
								if (mrow_lower_ [mrow_index]== -infinity_) {
									// If value is meaningful
									if (fabs (value) > smallElement_)
										mrow_lower_[mrow_index]
										    = mrow_upper_[mrow_index] - value;
								}
								else {
									duplicate = true;
								}
								break;
							}
							case MRMPS_G_ROW_CARD: {
								if (mrow_upper_[mrow_index] == infinity_) {
									// If value is meaningful
									if (fabs (value) > smallElement_)
										mrow_upper_[mrow_index]
										    = mrow_lower_[mrow_index] + value;
								}
								else {
									duplicate = true;
								}
								break;
							}
							case MRMPS_E_ROW_CARD: {
								if (mrow_lower_[mrow_index] == mrow_upper_[mrow_index]) {
									// If value is meaningful
									if (value > smallElement_) {
										mrow_upper_[mrow_index]
										    = mrow_upper_[mrow_index] + value;
									}
									else if (value < -smallElement_) {
										mrow_lower_[mrow_index]
										    = mrow_lower_[mrow_index] + value;
									}
								}
								else {
									duplicate = true;
								}
							}
						}

						// Error: duplicated mrng
						if (duplicate) {
							if (num_errors < 100) {
								handler_->message (COIN_MPS_DUPROW, messages_)
								        << mrmps_card_reader_->rowName()
								        << mrmps_card_reader_->cardNumber()
								        << mrmps_card_reader_->card()
								        << CoinMessageEol;
							}
							else if (num_errors > 100000) {
								handler_->message (COIN_MPS_RETURNING, messages_)
								        << CoinMessageEol;
								return num_errors;
							}
						}
					}
					// Error: no matching mrow
					else {
						num_errors++;
						if (num_errors < 100) {
							handler_->message (COIN_MPS_NOMATCHROW, messages_)
							        << mrmps_card_reader_->rowName()
							        << mrmps_card_reader_->cardNumber()
							        << mrmps_card_reader_->card()
							        << CoinMessageEol;
						}
						else if (num_errors > 100000) {
							handler_->message (COIN_MPS_RETURNING, messages_)
							        << CoinMessageEol;
							return num_errors;
						}
					}

					break;
				}
				// Error: bad image
				default: {
					num_errors++;
					if (num_errors < 100) {
						handler_->message (COIN_MPS_BADIMAGE, messages_)
						        << mrmps_card_reader_->cardNumber()
						        << mrmps_card_reader_->card() << CoinMessageEol;
					}
					else if (num_errors > 100000) {
						handler_->message (COIN_MPS_RETURNING, messages_)
						        << CoinMessageEol;
						return num_errors;
					}
				}
			}
		}
	}

	// If there is SROWS section
	if (mrmps_card_reader_->whichMrmpsSection() == MRMPS_SROWS_SECTION) {
		int max_srows = 1000;
		std::vector<MrmpsCardType> srow_types;
		srow_types.reserve (max_srows);

		while (mrmps_card_reader_->nextMrmpsField() == MRMPS_SROWS_SECTION) {
			switch (mrmps_card_reader_->whichMrmpsCard()) {
				case MRMPS_E_ROW_CARD:
				case MRMPS_L_ROW_CARD:
				case MRMPS_G_ROW_CARD: {
					// If capacity is reached, reserve memory
					if (num_srows_ >= max_srows) {
						max_srows = (3 * max_srows) / 2 + 1000;
						srow_types.reserve (max_srows);
					}

					srow_types.push_back (mrmps_card_reader_->whichMrmpsCard());
					srow_names_.insert (std::pair<std::string, int>
					                    (mrmps_card_reader_->cardName(), num_srows_));
					num_srows_++;

					break;
				}
				// Error: bad image
				default: {
					num_errors++;
					if (num_errors < 100) {
						handler_->message (COIN_MPS_BADIMAGE, messages_)
						        << mrmps_card_reader_->cardNumber()
						        << mrmps_card_reader_->card() << CoinMessageEol;
					}
					else if (num_errors > 100000) {
						handler_->message (COIN_MPS_RETURNING, messages_)
						        << CoinMessageEol;
						return num_errors;
					}
				}
			}
		}

		// Try reading SCOLS section
		if (mrmps_card_reader_->whichMrmpsSection() != MRMPS_SCOLS_SECTION) {
			handler_->message (COIN_MPS_BADIMAGE, messages_)
			        << mrmps_card_reader_->cardNumber()
			        << mrmps_card_reader_->card() << CoinMessageEol;
			handler_->message (COIN_MPS_RETURNING, messages_) << CoinMessageEol;
			return num_errors + 100000;
		}

		num_smat_elements_ = 0;
		int max_smat_elements = num_srows_ * 2;

		smat_srow_index_.reserve (max_smat_elements);
		smat_upar_index_.reserve (max_smat_elements);
		smat_element_.reserve (max_smat_elements);

		while (mrmps_card_reader_->nextMrmpsField() == MRMPS_SCOLS_SECTION) {
			switch (mrmps_card_reader_->whichMrmpsCard()) {
				case MRMPS_BLANK_COLUMN_CARD: {
					// Try getting upar index
					NameMap::iterator upar;
					upar = upar_names_.find (mrmps_card_reader_->columnName());

					if (upar != upar_names_.end()) {
						// Try getting srow index
						NameMap::iterator srow;
						srow = srow_names_.find (mrmps_card_reader_->rowName());

						if (srow != srow_names_.end()) {
							// If value is meaningful
							if (fabs (mrmps_card_reader_->value()) > smallElement_) {
								// If capacity is reached, reserve memory
								if (num_smat_elements_ >= max_smat_elements) {
									max_smat_elements = (3 * max_smat_elements) / 2 + 1000;
									smat_srow_index_.reserve (max_smat_elements);
									smat_upar_index_.reserve (max_smat_elements);
									smat_element_.reserve (max_smat_elements);
								}

								smat_srow_index_.push_back (srow->second);
								smat_upar_index_.push_back (upar->second);
								smat_element_.push_back (mrmps_card_reader_->value());
								num_smat_elements_++;
							}
						}
						// Error: no matching srow
						else {
							num_errors++;
							if (num_errors < 100) {
								handler_->message (COIN_MPS_NOMATCHROW, messages_)
								        << mrmps_card_reader_->rowName()
								        << mrmps_card_reader_->cardNumber()
								        << mrmps_card_reader_->card()
								        << CoinMessageEol;
							}
							else if (num_errors > 100000) {
								handler_->message (COIN_MPS_RETURNING, messages_)
								        << CoinMessageEol;
								return num_errors;
							}
						}
					}
					// Error: no matching upar
					else {
						num_errors++;
						if (num_errors < 100) {
							handler_->message (COIN_MPS_NOMATCHCOL, messages_)
							        << mrmps_card_reader_->columnName()
							        << mrmps_card_reader_->cardNumber()
							        << mrmps_card_reader_->card()
							        << CoinMessageEol;
						}
						else if (num_errors > 100000) {
							handler_->message (COIN_MPS_RETURNING, messages_)
							        << CoinMessageEol;
							return num_errors;
						}
					}

					break;
				}
				// Error: bad image
				default: {
					num_errors++;
					if (num_errors < 100) {
						handler_->message (COIN_MPS_BADIMAGE, messages_)
						        << mrmps_card_reader_->cardNumber()
						        << mrmps_card_reader_->card() << CoinMessageEol;
					}
					else if (num_errors > 100000) {
						handler_->message (COIN_MPS_RETURNING, messages_)
						        << CoinMessageEol;
						return num_errors;
					}
				}
			}
		}

		// Initialize srow bounds
		srow_lower_ = std::vector<double> (num_srows_, -infinity_);
		srow_upper_ = std::vector<double> (num_srows_, infinity_);

		// If there is SRHS section
		if (mrmps_card_reader_->whichMrmpsSection() == MRMPS_SRHS_SECTION) {
			while (mrmps_card_reader_->nextMrmpsField() == MRMPS_SRHS_SECTION) {
				switch (mrmps_card_reader_->whichMrmpsCard()) {
					case MRMPS_BLANK_COLUMN_CARD: {
						// Try getting srow index
						NameMap::iterator srow;
						srow = srow_names_.find (mrmps_card_reader_->rowName());

						if (srow != srow_names_.end()) {
							int srow_index = srow->second;
							int value = mrmps_card_reader_->value();

							// Check for duplicates
							if ( (srow_lower_[srow_index] == -infinity_)
							        && (srow_upper_[srow_index] == infinity_)) {
								// If value is meaningful
								if (fabs (mrmps_card_reader_->value())
								        > smallElement_) {
									switch (srow_types[srow_index]) {
										case MRMPS_E_ROW_CARD: {
											srow_lower_[srow_index] = value;
											srow_upper_[srow_index] = value;
											break;
										}
										case MRMPS_L_ROW_CARD: {
											srow_upper_[srow_index] = value;
											break;
										}
										case MRMPS_G_ROW_CARD: {
											srow_lower_[srow_index] = value;
										}
									}
								}
							}
							// Error: duplicated srhs
							else {
								if (num_errors < 100) {
									handler_->message (COIN_MPS_DUPROW, messages_)
									        << mrmps_card_reader_->rowName()
									        << mrmps_card_reader_->cardNumber()
									        << mrmps_card_reader_->card()
									        << CoinMessageEol;
								}
								else if (num_errors > 100000) {
									handler_->message (COIN_MPS_RETURNING, messages_)
									        << CoinMessageEol;
									return num_errors;
								}
							}
						}
						// Error: no matching srow
						else {
							num_errors++;
							if (num_errors < 100) {
								handler_->message (COIN_MPS_NOMATCHROW, messages_)
								        << mrmps_card_reader_->rowName()
								        << mrmps_card_reader_->cardNumber()
								        << mrmps_card_reader_->card()
								        << CoinMessageEol;
							}
							else if (num_errors > 100000) {
								handler_->message (COIN_MPS_RETURNING, messages_)
								        << CoinMessageEol;
								return num_errors;
							}
						}

						break;
					}
					// Error: bad image
					default: {
						num_errors++;
						if (num_errors < 100) {
							handler_->message (COIN_MPS_BADIMAGE, messages_)
							        << mrmps_card_reader_->cardNumber()
							        << mrmps_card_reader_->card() << CoinMessageEol;
						}
						else if (num_errors > 100000) {
							handler_->message (COIN_MPS_RETURNING, messages_)
							        << CoinMessageEol;
							return num_errors;
						}
					}
				}
			}
		}

		// Fill zero for blank srhs's
		for (int i = 0; i < num_srows_; i++) {
			switch (srow_types[i]) {
				case COIN_E_ROW: {
					if (srow_upper_[i] == infinity_
					        && srow_lower_[i] == -infinity_) {
						srow_upper_[i] = 0.0;
						srow_lower_[i] = 0.0;
					}
					break;
				}
				case COIN_L_ROW: {
					if (srow_upper_[i] == infinity_) {
						srow_upper_[i] = 0.0;
					}
					break;
				}
				case COIN_G_ROW: {
					if (srow_lower_[i] == -infinity_) {
						srow_lower_[i] = 0.0;
					}
					break;
				}
			}
		}

		// If there is SRANGES section
		if (mrmps_card_reader_->whichMrmpsSection() == MRMPS_SRANGES_SECTION) {
			while (mrmps_card_reader_->nextMrmpsField()
			        == MRMPS_SRANGES_SECTION) {
				switch (mrmps_card_reader_->whichMrmpsCard()) {
					case MRMPS_BLANK_COLUMN_CARD: {
						// Try getting srow index
						NameMap::iterator srow;
						srow = srow_names_.find (mrmps_card_reader_->rowName());

						if (srow != srow_names_.end()) {
							int srow_index = srow->second;
							int value = mrmps_card_reader_->value();

							// Check for duplicates
							bool duplicate = false;
							switch (srow_types[srow_index]) {
								case MRMPS_L_ROW_CARD:
									if (srow_lower_[srow_index] == -infinity_) {
										// If value is meaningful
										if (fabs (value) > smallElement_)
											srow_lower_[srow_index]
											    = srow_upper_[srow_index] - value;
									}
									else {
										duplicate = true;
									}
									break;
								case MRMPS_G_ROW_CARD:
									if (srow_upper_[srow_index] == infinity_) {
										// If value is meaningful
										if (fabs (value) > smallElement_)
											srow_upper_[srow_index]
											    = srow_lower_[srow_index] + value;
									}
									else {
										duplicate = true;
									}
									break;
								case MRMPS_E_ROW_CARD:
									if (srow_lower_[srow_index]
									        == srow_upper_[srow_index]) {
										// If value is meaningful
										if (value > smallElement_) {
											srow_upper_[srow_index]
											    = srow_upper_[srow_index] + value;
										}
										else if (value < -smallElement_) {
											srow_lower_[srow_index]
											    = srow_lower_[srow_index] + value;
										}
									}
									else {
										duplicate = true;
									}
							}

							// Error: duplicated srng
							if (duplicate) {
								if (num_errors < 100) {
									handler_->message (COIN_MPS_DUPROW, messages_)
									        << mrmps_card_reader_->rowName()
									        << mrmps_card_reader_->cardNumber()
									        << mrmps_card_reader_->card()
									        << CoinMessageEol;
								}
								else if (num_errors > 100000) {
									handler_->message (COIN_MPS_RETURNING, messages_)
									        << CoinMessageEol;
									return num_errors;
								}
							}
						}
						// Error: no matching srow
						else {
							num_errors++;
							if (num_errors < 100) {
								handler_->message (COIN_MPS_NOMATCHROW, messages_)
								        << mrmps_card_reader_->rowName()
								        << mrmps_card_reader_->cardNumber()
								        << mrmps_card_reader_->card()
								        << CoinMessageEol;
							}
							else if (num_errors > 100000) {
								handler_->message (COIN_MPS_RETURNING, messages_)
								        << CoinMessageEol;
								return num_errors;
							}
						}

						break;
					}
					// Error: bad image
					default: {
						num_errors++;
						if (num_errors < 100) {
							handler_->message (COIN_MPS_BADIMAGE, messages_)
							        << mrmps_card_reader_->cardNumber()
							        << mrmps_card_reader_->card() << CoinMessageEol;
						}
						else if (num_errors > 100000) {
							handler_->message (COIN_MPS_RETURNING, messages_)
							        << CoinMessageEol;
							return num_errors;
						}
					}
				}
			}
		}
	}

	// Initialize uncertain parameter bounds
	upar_lower_ = std::vector<double> (num_upars_, 0);
	upar_upper_ = std::vector<double> (num_upars_, infinity_);

	// If there is SBOUNDS section
	if (mrmps_card_reader_->whichMrmpsSection() == MRMPS_SBOUNDS_SECTION) {
		while (mrmps_card_reader_->nextMrmpsField() == MRMPS_SBOUNDS_SECTION) {
			switch (mrmps_card_reader_->whichMrmpsCard()) {
				case MRMPS_LO_BOUND_CARD:
				case MRMPS_UP_BOUND_CARD:
				case MRMPS_FX_BOUND_CARD:
				case MRMPS_FR_BOUND_CARD:
				case MRMPS_MI_BOUND_CARD:
				case MRMPS_PL_BOUND_CARD: {
					// Try getting upar index
					NameMap::iterator upar;
					upar = upar_names_.find (mrmps_card_reader_->columnName());

					if (upar != upar_names_.end()) {
						int upar_index = upar->second;
						double value = mrmps_card_reader_->value();

						switch (mrmps_card_reader_->whichMrmpsCard()) {
							case MRMPS_LO_BOUND_CARD: {
								// If value is meaningful
								if (fabs (value) > smallElement_) {
									upar_lower_[upar_index] = value;
								}
								else {
									upar_lower_[upar_index] = 0;
								}
								break;
							}
							case MRMPS_UP_BOUND_CARD: {
								// If value is meaningful
								if (fabs (value) > smallElement_) {
									upar_upper_[upar_index] = value;
								}
								else {
									upar_upper_[upar_index] = 0;
								}
								break;
							}
							case MRMPS_FX_BOUND_CARD: {
								// If value is meaningful
								if (fabs (value) > smallElement_) {
									upar_lower_[upar_index] = value;
									upar_upper_[upar_index] = value;
								}
								else {
									upar_lower_[upar_index] = 0;
									upar_upper_[upar_index] = 0;
								}
								break;
							}
							case MRMPS_FR_BOUND_CARD: {
								upar_lower_[upar_index] = -infinity_;
								upar_upper_[upar_index] = infinity_;
								break;
							}
							case MRMPS_MI_BOUND_CARD: {
								upar_lower_[upar_index] = -infinity_;
								upar_upper_[upar_index] = 0;
								break;
							}
							case MRMPS_PL_BOUND_CARD: {
								upar_lower_[upar_index] = 0;
								upar_upper_[upar_index] = infinity_;
								break;
							}
						}
					}
					// Error: no matching upar
					else {
						num_errors++;
						if (num_errors < 100) {
							handler_->message (COIN_MPS_NOMATCHCOL, messages_)
							        << mrmps_card_reader_->columnName()
							        << mrmps_card_reader_->cardNumber()
							        << mrmps_card_reader_->card()
							        << CoinMessageEol;
						}
						else if (num_errors > 100000) {
							handler_->message (COIN_MPS_RETURNING, messages_)
							        << CoinMessageEol;
							return num_errors;
						}
					}

					break;
				}
				// Error: bad image
				default: {
					num_errors++;
					if (num_errors < 100) {
						handler_->message (COIN_MPS_BADIMAGE, messages_)
						        << mrmps_card_reader_->cardNumber()
						        << mrmps_card_reader_->card() << CoinMessageEol;
					}
					else if (num_errors > 100000) {
						handler_->message (COIN_MPS_RETURNING, messages_)
						        << CoinMessageEol;
						return num_errors;
					}
				}
			}
		}
	}

	// Try reading ENDATA section
	if (mrmps_card_reader_->whichMrmpsSection() != MRMPS_ENDATA_SECTION) {
		handler_->message (COIN_MPS_BADIMAGE, messages_)
		<< mrmps_card_reader_->cardNumber()
		<< mrmps_card_reader_->card() << CoinMessageEol;
		handler_->message (COIN_MPS_RETURNING, messages_) << CoinMessageEol;
		return num_errors + 100000;
	}

	return num_errors;
}