/**
 * @file MrmpsCardReader.cpp
 * @brief
 *
 * @author Changhyeok Lee
 * @date Dec 24, 2013
 */

#include "MrmpsCardReader.hpp"

const static char* mrmps_section[] = {
	"",
	"NAME",
	"STAGES",
	"COLSTGS",
	"ROWSTGS",
	"UCOEFFS",
	"UPARS",
	"UPARMAP",
	"MROWS",
	"MCOLS",
	"MRHS",
	"MRANGES",
	"SROWS",
	"SCOLS",
	"SRHS",
	"SRANGES",
	"SBOUNDS",
	"ENDATA",
	" ",
	" "
};

const static char* mrmps_card[] = {
	"E",
	"L",
	"G",
	"LO",
	"UP",
	"FX",
	"FR",
	"MI",
	"PL",
	"  ",
	"  "
};

const static char* blanks = (const char*) " \t";

const static MrmpsCardType start_card[] = {
	MRMPS_UNKNOWN_CARD,
	MRMPS_UNKNOWN_CARD,
	MRMPS_BLANK_COLUMN_CARD,
	MRMPS_BLANK_COLUMN_CARD,
	MRMPS_BLANK_COLUMN_CARD,
	MRMPS_BLANK_COLUMN_CARD,
	MRMPS_BLANK_COLUMN_CARD,
	MRMPS_BLANK_COLUMN_CARD,
	MRMPS_E_ROW_CARD,
	MRMPS_BLANK_COLUMN_CARD,
	MRMPS_BLANK_COLUMN_CARD,
	MRMPS_BLANK_COLUMN_CARD,
	MRMPS_E_ROW_CARD,
	MRMPS_BLANK_COLUMN_CARD,
	MRMPS_BLANK_COLUMN_CARD,
	MRMPS_BLANK_COLUMN_CARD,
	MRMPS_LO_BOUND_CARD,
	MRMPS_UNKNOWN_CARD,
	MRMPS_UNKNOWN_CARD,
	MRMPS_UNKNOWN_CARD,
};

const static MrmpsCardType end_card[] = {
	MRMPS_UNKNOWN_CARD,
	MRMPS_UNKNOWN_CARD,
	MRMPS_UNKNOWN_CARD,
	MRMPS_UNKNOWN_CARD,
	MRMPS_UNKNOWN_CARD,
	MRMPS_UNKNOWN_CARD,
	MRMPS_UNKNOWN_CARD,
	MRMPS_UNKNOWN_CARD,
	MRMPS_LO_BOUND_CARD,
	MRMPS_UNKNOWN_CARD,
	MRMPS_UNKNOWN_CARD,
	MRMPS_UNKNOWN_CARD,
	MRMPS_LO_BOUND_CARD,
	MRMPS_UNKNOWN_CARD,
	MRMPS_UNKNOWN_CARD,
	MRMPS_UNKNOWN_CARD,
	MRMPS_BLANK_COLUMN_CARD,
	MRMPS_UNKNOWN_CARD,
	MRMPS_UNKNOWN_CARD,
	MRMPS_UNKNOWN_CARD,
};

MrmpsCardReader::MrmpsCardReader (CoinFileInput* input, CoinMpsIO* reader) :
	CoinMpsCardReader (input, reader),
	mrmps_section_type_ (MRMPS_NO_SECTION),
	mrmps_card_type_ (MRMPS_BLANK_COLUMN_CARD),
	card_name_() {
}

MrmpsCardReader::~MrmpsCardReader() {
}

const MrmpsSectionType MrmpsCardReader::nextMrmpsField() {
	while (1) {
		// Read new card
		if (cleanCard()) {
			return MRMPS_EOF_SECTION;
		}

		// If card starts with space, then process card
		if (card_[0] == ' ' || card_[0] == '\t' || card_[0] == '\0' || card_[0] == '\n' || card_[0] == '\r') {
			// Initialize card pointers
			position_ = card_;
			eol_ = card_ + strlen (card_);

			char card[COIN_MAX_FIELD_LENGTH];
			strcpy(card, card_);

			// Read first non-blank field
			char* field = strtok (card, blanks);

			// If there is non-blank, then process card
			if (field) {
				// If MROWS, SROWS, or SBOUNDS section,
				// then try reading first field as type
				if (mrmps_section_type_ == MRMPS_MROWS_SECTION
				    || mrmps_section_type_ == MRMPS_SROWS_SECTION
				    || mrmps_section_type_ == MRMPS_SBOUNDS_SECTION) {

					int i;
					for (i = start_card[mrmps_section_type_];
					        i < end_card[mrmps_section_type_]; i++) {
						// If correct type is found,
						// then save card type and scan to second field
						if (!strcmp (field, mrmps_card[i])) {
							mrmps_card_type_ = static_cast<MrmpsCardType> (i);

							// Read second field
							field = strtok (0, blanks);

							// If there is no second field, then error
							if (!field) {
								position_ = eol_;
								mrmps_card_type_ = MRMPS_UNKNOWN_CARD;

								return mrmps_section_type_;
							}

							break;
						}
					}
					// If type field is not found, then error
					if (i == end_card[mrmps_section_type_]) {
						position_ = eol_;
						mrmps_card_type_ = MRMPS_UNKNOWN_CARD;

						return mrmps_section_type_;
					}
				}
				// If not MROWS, SROWS, or SBOUNDS section
				// then card type is MRMPS_BLANK_COLUMN_CARD
				else {
					mrmps_card_type_ = MRMPS_BLANK_COLUMN_CARD;
				}

				switch (mrmps_section_type_) {
					case MRMPS_STAGES_SECTION: {
						// Copy second field to columnName_
						strcpy (columnName_, field);

						// Read third field
						field = strtok (0, blanks);

						// If there is third field, then copy to rowName_
						if (field) {
							strcpy (rowName_, field);
						}
						// If no third field, then error
						else {
							position_ = eol_;
							mrmps_card_type_ = MRMPS_UNKNOWN_CARD;

							return mrmps_section_type_;
						}

						break;
					}
					case MRMPS_COLSTGS_SECTION: {
						// Copy second field to columnName_
						strcpy (columnName_, field);

						// Read third field
						field = strtok (0, blanks);

						// If there is third field, then copy to value_
						if (field) {
							value_ = strtod (field,0);
						}
						// If no third field, then error
						else {
							position_ = eol_;
							mrmps_card_type_ = MRMPS_UNKNOWN_CARD;

							return mrmps_section_type_;
						}

						break;
					}
					case MRMPS_UCOEFFS_SECTION: {
						// Copy second field to card_name_
						strcpy (card_name_, field);

						// Read third field
						field = strtok (0, blanks);

						// If there is third field, then copy to columnName_
						if (field) {
							strcpy (columnName_, field);
						}
						// If no third field, then error
						else {
							position_ = eol_;
							mrmps_card_type_ = MRMPS_UNKNOWN_CARD;

							return mrmps_section_type_;
						}

						// Read fourth field
						field = strtok (0, blanks);

						// If there is fourth field, then copy to rowName_
						if (field) {
							strcpy (rowName_, field);
						}
						// If no fourth field, then error
						else {
							position_ = eol_;
							mrmps_card_type_ = MRMPS_UNKNOWN_CARD;

							return mrmps_section_type_;
						}

						break;
					}
					case MRMPS_UPARS_SECTION: {
						// Copy second field to card_name_
						strcpy (card_name_, field);

						// Read third field
						field = strtok (0, blanks);

						// If there is third field, then copy to value_
						if (field) {
							value_ = strtod (field,0);
						}
						// If no third field, then error
						else {
							position_ = eol_;
							mrmps_card_type_ = MRMPS_UNKNOWN_CARD;

							return mrmps_section_type_;
						}

						break;
					}
					case MRMPS_UPARMAP_SECTION:
					case MRMPS_MCOLS_SECTION:
					case MRMPS_SCOLS_SECTION:{
						// Copy second field to columnName_
						strcpy (columnName_, field);

						// Read third field
						field = strtok (0, blanks);

						// If there is third field, then copy to rowName_
						if (field) {
							strcpy (rowName_, field);
						}
						// If no third field, then error
						else {
							position_ = eol_;
							mrmps_card_type_ = MRMPS_UNKNOWN_CARD;

							return mrmps_section_type_;
						}

						// Read fourth field
						field = strtok (0, blanks);

						// If there is fourth field, then copy to value_
						if (field) {
							value_ = strtod (field,0);
						}
						// If no fourth field, then error
						else {
							position_ = eol_;
							mrmps_card_type_ = MRMPS_UNKNOWN_CARD;

							return mrmps_section_type_;
						}

						break;
					}
					case MRMPS_MROWS_SECTION:
					case MRMPS_SROWS_SECTION:  {
						// Copy second field to card_name_
						strcpy (card_name_, field);

						break;
					}
					case MRMPS_ROWSTGS_SECTION:
					case MRMPS_MRHS_SECTION:
					case MRMPS_MRANGES_SECTION:
					case MRMPS_SRHS_SECTION:
					case MRMPS_SRANGES_SECTION:{
						// Copy second field to rowName_
						strcpy (rowName_, field);

						// Read third field
						field = strtok (0, blanks);

						// If there is third field, then copy to value_
						if (field) {
							value_ = strtod (field,0);
						}
						// If no third field, then error
						else {
							position_ = eol_;
							mrmps_card_type_ = MRMPS_UNKNOWN_CARD;

							return mrmps_section_type_;
						}

						break;
					}
					case MRMPS_SBOUNDS_SECTION: {
						// Copy second field to columnName_
						strcpy (columnName_, field);

						switch (mrmps_card_type_) {
							case MRMPS_LO_BOUND_CARD:
							case MRMPS_UP_BOUND_CARD:
							case MRMPS_FX_BOUND_CARD: {
								// Read third field
								field = strtok (0, blanks);

								// If there is third field, then copy to value_
								if (field) {
									value_ = strtod (field,0);
								}
								// If no third field, then error
								else {
									position_ = eol_;
									mrmps_card_type_ = MRMPS_UNKNOWN_CARD;

									return mrmps_section_type_;
								}

								break;
							}
							case MRMPS_FR_BOUND_CARD:
							case MRMPS_MI_BOUND_CARD:
							case MRMPS_PL_BOUND_CARD: {
								// Do nothing
								break;
							}
							// Shouldn't reach here. If so, error
							default: {
								position_ = eol_;
								mrmps_card_type_ = MRMPS_UNKNOWN_CARD;

								return MRMPS_SBOUNDS_SECTION;
							}
						}

						break;
					}
					// Shouldn't reach here. If so, error
					default: {
						position_ = eol_;
						mrmps_card_type_ = MRMPS_UNKNOWN_CARD;

						return MRMPS_UNKNOWN_SECTION;
					}
				}

				// Read next field: must be NULL
				field = strtok (0, blanks);

				// If field is NULL, then exit while-loop
				if (!field) {
					break;
				}
				// If field is not NULL, then error
				else {
					position_ = eol_;
					mrmps_card_type_ = MRMPS_UNKNOWN_CARD;

					return mrmps_section_type_;
				}
			}
			// If blank, continue
			else {
				continue;
			}
		}
		// If comment card, continue
		else if (card_[0] == '*') {
			continue;
		}
		// If mrmps_section card, empty card and return section type
		else {
			// Find mrmps_section type
			int i;
			handler_->message (COIN_MPS_LINE, messages_) << cardNumber_
			        << card_ << CoinMessageEol;
			for (i = MRMPS_STAGES_SECTION; i < MRMPS_UNKNOWN_SECTION; i++) {
				if (!strcmp (card_, mrmps_section[i])) {
					break;
				}
			}

			// Empty card
			position_ = card_;
			eol_ = card_;

			// Return mrmps_section type
			mrmps_section_type_ = static_cast<MrmpsSectionType> (i);
			return mrmps_section_type_;
		}
	}

	// Return current mrmps_section type
	return mrmps_section_type_;
}
